<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <div id="app">
        <h1>List Belanjaan</h1><br> 
        <input type="text" v-model="nama" required>
        <input type="text" v-model="jumlah" required>
        <button v-on:click="addCart">Create</button>

        <br><br>
        <ul v-for="(cart, i) in carts">
            <li>
                @{{ cart.nama }} => Jumlah : @{{ cart.jumlah }}
                <button v-if="cart.jumlah > 1" v-on:click="updateCart(cart.jumlah-- - 1, cart.id)">-</button>
                <button v-on:click="updateCart(cart.jumlah++ + 1, cart.id)">+</button>
                <button v-on:click="removeCart(cart.id, i)">X</button>
            </li>
        </ul>

    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="{{ asset('assets/vue-resource.js') }}"></script>

    <script>
        var app = new Vue({
            el: '#app',
            data() {
                return {
                    carts: [],
                    nama:'',
                    jumlah: '',
                }
            },
            methods: {
                addCart: function (){
                    let namecart = this.nama.trim();
                    let jumlahcart = this.jumlah.trim();
                     this.$http.post('/api/cart', {nama: namecart, jumlah: jumlahcart}).then(response => {

                       this.$http.get('/api/cart').then(response => {
                        // get body data
                        this.carts = response.body.data;
                        this.nama = '',
                        this.jumlah = ''
                });

                    });
                },
                updateCart: function(cart, i) {
                    this.$http.patch('/api/cart/' + i,{jumlah: cart}).then(response => {
                    // get body data
                    this.$http.get('/api/cart').then(response => {
                        // get body data
                        this.carts = response.body.data;
                        });
                });
                },
                removeCart: function(cart, i) {
                    this.$http.delete('/api/cart/' + cart).then(response => {
                        this.carts.splice(i, 1);
                });
                }
            },
            mounted() {
                // GET /someUrl
                this.$http.get('/api/cart').then(response => {
                    // get body data
                    this.carts = response.body.data;
                });
                
            },
        })
    </script>
</body>
</html>